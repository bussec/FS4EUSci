**********************************
Free Software for European Science
**********************************

Summary
=======

The principles of Free Software (FS) appear to be well-aligned with the core tenets of science, i.e., the sharing of knowledge and the demand for reproducibility of results. Nevertheless, FS is still rather the exception than the rule for the majority of scientific domains. The aim of the following text is to provide an introduction to the state of FS within the European scientific community. As European science is predominantly tax-funded, it will mainly focus on the generation of FS by scientists as part of their research activity and only briefly discuss the use of FS within scientific institutions. The text will give an overview about current requirements and recommendations of European and national funding agencies and possible points for an "FS4EuroSci" agenda.

Note 1: The text is potentially biased towards Life Sciences. Input from other domains is highly welcome.

Note 2: The text is primarily written for an audience familiar with the details of the Free Software concept. Other readers might want to read `Appendix A`_ first.

.. _`Appendix A`: `Appendix A - The concept of Free Software`_

Background
==========

Modern science considers the reproducibility of results to be an indispensible requirement for their acceptance by the community. Therefore it is considered to be a legitimate interest to analyze, reproduce and build upon the research output of others as long as there is no undue appropriation and proper credit is provided - typically by the citation of the original work. Over the last two decades, digitalization has started to transform a large number of scientific domains, both due to the possiblity to generate large bodies of digitalized data as well as the possibilty to share/publish them. The size and complexity of these data sets has made manual (often spreadsheet-based) analysis and management prohibitive and therefore requires more and more elaborate software tools. Importantly, the use FS is by no means the standard in this setting, although the interests of the scientists (as stated above) would be reflected by the 4 Freedoms of FS.

Academic science in Europe is primarily tax-funded with an estimated annual budget of 50--75 billion EUR [#]_ . The EU had started to promote the idea of Open Science which state the easy access to tax-funded research output. This is usually represented by the term `FAIR`_ (findable, accessible, interoperable, reuseable).

.. [#] The 3% GDP target is not met by the majority of EU member states and includes also industrial R&D, which actually makes up the majority of R&D expenses.

.. _`FAIR`: https://www.go-fair.org/fair-principles/


General Issues
==============

FAIR has by now become a universally understood (although not universally practiced) concept in regard to scientific data. However, while the FAIR principle can in also be applied to code (i.e., software), it is important to recognize a couple of points here:

*  Code can be FAIR but not FS (e.g., FAIR does not require an FS license)
*  Code can be FS but not FAIR (especially *Findability* and *Interoperability* can be problematic)
*  In contrast to data, code invites contributors and thus will change over time. This is something that the current system of scholorly communication does not provide credit for, as discussed `here`_.
*  There is little knowledge about licensing and the impacts on further usage.
*  The concept of dependencies between licences for different objects already is already present in FAIR, as GP-I2 formulates that vocabularies used by data objects must also comply to the FAIR criteria, so that interchangeability is guaranteed.
*  The issue of software sustainability has been analyzed e.g. by [Howison_2016]_.

.. _`here`: https://www.slideshare.net/danielskatz/fair-is-not-fair-enough-particularly-for-software-citation-availability-or-quality


European Policies
=================

Current developments
--------------------

*  The `EOSC Declaration Action List`_ states that "Software sustainability should be treated on an equal footing as data stewardship".
*  The "Horizon Europe Regulation Proposal" of the European Commission (EC) [COM_2018_435]_ mentions under point (5) that within the program "Provisions should be laid down to ensure that beneficiaries provide open access to peer-reviewed scientific publications, research data and other research outputs in an open and non-discriminatory manner, free of charge and as early as possible in the dissemination process, and to enable their widest possible use and re-use." This notion is implemented in Article 10. While "software" as a research output is only specifically mentioned in Annex V of the document, the case can be made that this is implicitly included. As usual the EU is rather lenient in providing exceptions from Open Science guidelines if there is a possibility to commercialize.
*  The final report and action plan "Turning FAIR into reality" by the EC expert group on FAIR data [DOI_10_2777_1524]_ mentions ``code`` or ``software`` as an important research output. It also briefly disucsses the issues of software sustainability and mentions the RSE movement. However, it should be noted that ``code`` or ``software`` is primarily mentioned in enumerations with ``data`` and ``metadata``, but more special aspects are not discussed. FS, OSS or software licensing are not explicitly discussed in the text.
*  `Plan S`_ does mention software as separate entity, however only in the more detailed section, not in the 10 principles.


.. _`EOSC Declaration Action List`: https://ec.europa.eu/research/openscience/pdf/eosc_declaration-action_list.pdf
.. _`Plan S`: https://www.coalition-s.org/principles-and-implementation/

National Policies
=================

Germany
-------

*  DFG has now released (07/19) an updated document for their recommendation for Good Scientific Practice (GSP) [#]_ , which includes recommandation on research software: [NOT_YET_READ] https://www.dfg.de/download/pdf/foerderung/rechtliche_rahmenbedingungen/gute_wissenschaftliche_praxis/kodex_gwp.pdf
*  DFG had a call in 2016 for projects increase software sustainability in the research context. The call is rather broad, dealing with infrastructures and best practices, FLOSS is not specifcially mentioned: https://www.dfg.de/foerderung/info_wissenschaft/2016/info_wissenschaft_16_71/index.html
*  There is a follow-up call on Software Sustainablility, which also addresses open source ("quelloffen") as an outcome and a target in terms of sustainability. However, it does not require a FS license. https://www.dfg.de/foerderung/info_wissenschaft/2019/info_wissenschaft_19_44/index.html
*  Helmholtz is starting to address this issue in the context of its Open Science Working Group:

   *  Positionspapier "Zugang zu und Nachnutzung von wissenschaftlicher Software" (2017) [https://os.helmholtz.de/index.php?id=2766]
      This document summarizes a workshop in Dresden (11/2016) that was organized by HGFOS and included people from HGF, MPG, DFG and other. The *Allianz* ad-hoc group was founded here. In general the document deals with sustainablity (Guidelines, Incentives, Publication & Appreciation, Infrastructure (Repos), Educational/Personal, Legal framework). It is noteworthy that both the "Incentives" and "Legal" sections **explicitly** recommend/deal with OSS. Otherwise the document recommends the centers to build up structures and processes that help scientist ensure software sustainability. Note that this is no a binding policy document, more a starting point for the centers to develop their own guidelines.
   *  HGFOS Workshop "Zugang zu und Nachnutzung von wissenschaftlicher Software" (2017) [http://doi.org/10.2312/lis.17.01]
      Also a product of the Dresden Workshop (s.a.), this report is more extensive than the original position paper. Also, in regard to software developed by researchers, this report is substantially more comprehensive than Katerbow *et al.*. It has in general a positive sentiment towards FS, but allows for closed soure in case of commercialization.
   *  Diskussionspapier "Empfehlungen zur Implementierung von Leit- und Richtlinien [...]" https://os.helmholtz.de/index.php?id=3197
      This document is also available in a slightly abridged English version "Dealing with Research Software [...]" https://doi.org/10.2312/os.helmholtz.003
      A rather detailed document that should act as a basis for discussion for the the development of the model guidelines for research software management at HGF centers. Contains a recommendation for the OSI-compliant use of OSI-compliant licenses, along with a case-by-case decision for permissive vs. share-alike. Does not discuss the relation between FAIR and FS.
   *  A model guideline ("Musterrichtlinie") for research software management is currently under development by HGFOS. Contact: K. Scheliga, HGFOS @ GfZ

*  NFDI has a clear requirement for open source (p. 7): https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf

*  Allianz der deutschen Wissenschaftsorganisationen (*Allianz*) -> Schwerpunktinitiative "Digitale Information" -> Ad-hoc AG "Wissenschaftliche Software"
   *  Katerbow M et al. (03/2018) Recommendations on the development, use and provision of Research Software.

      *  English version https://doi.org/10.5281/zenodo.1172988
      *  German version https://doi.org/10.5281/zenodo.1172970

      This document provides high level recommendations on Research Software. In general the tone is rather positive towards FS, e.g. the use of OSS is explicitly recommended. For development, the text is less explicit, i.e. it says that source code SHOULD be readable and MIGHT/SHOULD be licenced under an FS licence, however potential commercialization is named as the key exception to this rule.

   *  Ad-hoc WG now moved into "Phase 3" and is now a real WG "Digitale Werkzeuge: Software und Dienste"
   *  Deliverable: Detailed recommendation document (Planing / Tools / Publications / Infrastructure)

*  In terms of use, there is no indication that FS is practicular relevant. This is complicated by the fragmentation within the 16 Länder (states). The "Expertenkommision Forschung und Innovation" (Expert committee research and innovation) [established by the federal gov't (BReg) in 2016], reports extensively on the challenges of digitalization faced by universities, but does not mention FS once. On the contrary, it recommends a coordinated approach to procure (commercial) software licenses. EFI Gutachten 2019, pp. 101 & 104 https://www.e-fi.de/fileadmin/Gutachten_2019/EFI_Gutachten_2019.pdf

.. [#] de: Gute wissenschaftliche Praxis, GWP

Still to check
==============

*  Geosciences and FS: https://www.listserv.dfn.de/sympa/arc/wiss-software/2019-01/msg00000.html
*  OSS am DLR: There is an *internal only* Brochure that is the authorative guideline, but the following presentation captures a couple of points
   http://elib.dlr.de/100060/1/20151018%2520Open%2520Source%2520im%2520DLR.pdf
*  From RSE19 "FAIR Software" BoF session: https://librarycarpentry.org/Top-10-FAIR/2018/12/01/research-software/


Other stakeholders
==================

*  Research Software Engineer (RSE) organisations: Originating from the UK in ~2014, these organisations promote that RSEs should be recognized and paid comparable to classical researcher roles. This goes along with the demand that software should be recognized as a primary research product and thus requires professionalisation of its development and maintenance process. While the organisations are typically sympathetic to the Free Software movement, it has to be recognized that this is not necessary a primary focus, but rather a derivative of the interest in software sustainability.

   *  Jon Switters, David Osimo; Recognising the Importance of Software in Research -- Research Software Engineers (RSEs), a UK Example; Jan 2019
      https://ec.europa.eu/info/sites/info/files/research_and_innovation/importance_of_software_in_research.pdf

*  Software Sustainability Institutes are organisation closely entangeled with the RSE movement and address the question how scientific software development can be made open and provide long-term maintenance:


Potential points on the policy agenda
=====================================

*  The FAIR principles were formulated with data in mind, not software. => Provide input for potential amendments for software objects in general, to ensure that these are not "black boxes" but FS.
*  Github is frequently used to deposit software, however there are a number of implications:

   *  The long-term perservation and assignment of DOI is incomplete. The possibility to push releases to Zenodo (which then will provide a DOI) exists, however this only creates a ZIP of the current code (*not* the commit tree).
   *   Everything outside of the core git functionality stays on a proprietary platform (bug reports. implementation decisions, etc.).

   Therefore funding acencies should be encouraged to deploy own code repositories (e.g. by enhancing GitLab or similar FS repos) that guarantee the continued availability of this form of ScholCom.

*  It is time for Free + FAIR research software: This means in essence that the FS princples that are currently not explicitly covered by FAIR need to be included in a revised version. Whether this still sails under the FAIR banner is not the key question, but it would be nice if there is a clear distinction in the name (FAIR 2.0). As FAIR is a rather operational frame work, it might be a good idea to have a look at the OSS criteria, as they might in certain points be more tangible than the 4 Freedoms.
*  We need an incentivation scheme for professional software maintenance that does allow coders to stay in science (and not go for a start-up). In simple terms this can be done via authorship (in addition to citation). This is a topic that would likely go alongside with RSE.
*  A lot of policy documents consider the FS vs proprietary licensing a decision that is up to the researcher. The EU Open Science Policy Pilot is at least at the point that FS is the default but with wide ranging opt-outs. The critical question is: How many *good* examples for proprietary licensing giving rise to a viable business are there? How many are there for dual-licensing or FS? 
* Another point for discussion is the use of "open-source-but-propriatary" licenses (instead of "closed-source-and-propriatary"). While FS is of course the goal, from a scientific perspective a open source that cannot be used is better than a black-box. However, it must be clear that this is pitched as an alternative to propriatary licensing, only. It should never be perceived as an alternative to FS licenses if they can be applied to the code.


Terms
=====

*  "FAIR": Acronym for "Findable, Accessible, Interoperable, Reuseable". Originally developed for data.

* "long tail of science": Refers to the 80% of research done by groups that only see 20% of the ICT budget: Experimental groups, small universities, humanities, etc.. As long as solutions are only made to meet the needs of large consortia and "light-house" projects, this research will continue to be done in spreadsheet applications.


Events
======

*  Open Access Tage -> https://open-access.net/AT-DE/community/open-access-tage/


Further Reading
===============

*  FORCE11 had a temporary workgroup on software citation in 2016. Their report mainly recommends the use of PIDs that optimally should be version specific and appropriate credit by metadata. There not recommendation on licensing. DOI: https://doi.org/10.7717/peerj-cs.86
*  The Netherlands eScience Center is running a research software index, which attempt to address the "F" in FAIR: https://www.research-software.nl/about
*  The German Federal Ministry for Education and Research has a program called "Software-Sprint" [https://www.bmbf.de/foerderungen/bekanntmachung-1225.html], which funds Open Source Projects by free-lance programmers focusing on security or civic tech. Note that institutions like universities are excluded from application, neverthe this could be interesting in special cases.


References
==========

.. [Howison_2016] James Howison and Julia Bullard. 2016. Software in the scientific literature: Problems with seeing, finding, and using software mentioned in the biology literature. Journal of the Association for Information Science and Technology 67, 9 (2016), 2137–2155. https://doi.org/10.1002/asi.23538

.. [COM_2018_435] https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=COM:2018:435:FIN

.. [DOI_10_2777_1524] https://doi.org/10.2777/1524


Appendix
========

Appendix A - The concept of Free Software
-----------------------------------------

The term "Free Software" was coined by Richard Stallman in 1986 to describe software that provides the users with the "`4 freedoms`_", meaning the freedom to:

*  *Use:* Run the program for any purpose
*  *Study:* Understand how a program works
*  *Share:* Redistribute copies of a program
*  *Enhance:* Change a program to suit your needs and share the modified version with others

With minor revisions the original definition is still used by both the US-based Free Software Foundation (FSF) and the Free Software Foundation Europe (FSFE). Note that while not explicitly mentioned, the user requires access to the source code for the second and fourth point to be fulfilled. Further note that it is implicitly assumed that the 4 Freedoms are granted via a license.

The related term "Open Source Software" was coined by the Open Source Initiative (OSI) starting 1998 to avoid the dual meaning of "free" as in "at no cost", which was considered problematic from economic view point. The OSI's `Open Source Definition`_ contains 10 criteria that have a stronger focus on the license of the software than on the user.

Nevertheless, in practice both terms describe - by and large - the same set of licenses. Importantly, all frequently used licences are approved both by the FSF and the OSI. Therefore this text gives preference to the older termer, which - in addition - is easier to map to the scientific process

.. _`4 Freedoms`: https://www.gnu.org/philosophy/free-sw.html
.. _`Open Source Definition`: https://opensource.org/osd
